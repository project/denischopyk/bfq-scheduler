Для выбора планировщика ввода/вывода BFQ выполним следующие действия:

Добавить строку из трех букв "**bfq**" в конец файла  **/etc/initramfs-tools/modules**  

```bash
sudo nano /etc/initramfs-tools/modules
```

```bash
# List of modules that you want to include in your initramfs.
# They will be loaded at boot time in the order below.
#
# Syntax:  module_name [args ...]
#
# You must run update-initramfs(8) to effect this change.
#
# Examples:
#
# raid1
# sd_mod
bfq
```

Создаём файл с правилами выбора планировщика:

```bash
sudo nano /etc/udev/rules.d/60-ioschedulers.rules
```

```bash
#set scheduler for NVMe

ACTION=="add|change", KERNEL=="nvme[0-9]n[0-9]", ATTR{queue/scheduler}="none"

# set scheduler for SSD and eMMC

ACTION=="add|change", KERNEL=="sd[a-z]*|mmcblk[0-9]*", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="mq-deadline"

# set scheduler for rotating disks

ACTION=="add|change", KERNEL=="sd[a-z]*", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"
```

Далее в  **/etc/default/grub** в кавычки добавляем строку 

**scsi_mod.use_blk_mq=1**

```bash
sudo nano /etc/default/grub
```

```bash
GRUB_CMDLINE_LINUX_DEFAULT="...блаблабла...scsi_mod.use_blk_mq=1"
```

После сохранения файла выполнить команду:

```bash
sudo update-grub
```

После чего выполнить reboot  и после включения выполнить:

```bash
cat /sys/block/sda/queue/scheduler
```

```bash
dd@astra ~/D/linux> cat /sys/block/sda/queue/scheduler
mq-deadline [bfq] none
```

Должно быть [**bfq**].

И на этом всё! Готово)


